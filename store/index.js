import axios from 'axios'

import Vuex from 'vuex'

const createrStore = () => {
  return new Vuex.Store({
    state: {
      decks: [],
    },
    mutations: {
      setDecks(state, decks) {
        state.decks = decks
      },
    },
    actions: {
      nuxtServerInit(vuexContext, context) {
        return axios
          .get(
            'https://nuxt-learning-english-4a130-default-rtdb.firebaseio.com/decks.json'
          )
          .then((response) => {
            const deckArray = []
            for (const key in response.data) {
              deckArray.push({ ...response.data[key], id: key })
            }

            vuexContext.commit('setDecks', deckArray)
          })
          .catch((e) => {
            vuexContext.error(e)
          })
      },
      setDecks(vuexContext, decks) {
        vuexContext.commit = decks
      },
    },
    getters: {
      decks(state) {
        return state.decks
      },
    },
  })
}
export default createrStore
